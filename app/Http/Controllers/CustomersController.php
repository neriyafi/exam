<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Customer;
use App\User;
use Illuminate\Support\Facades\Gate;

class CustomersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $id = Auth::id();
        $users = User::All();
        $customers = Customer::All(); 
        return view('customers.index', compact('customers','id'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('customers.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $customers = new customer(); 
        $users =  User::findOrFail(Auth::id());  
        $customers->name = $request->name; 
        $customers->email = $request->email;
        $customers->phone = $request->phone;
        $customers->user_id = Auth::id();
        $customers->user_name = $users->name;
       // $customers->created_at = date('Y,m,d G:i:s');
        $customers->save();
        return redirect('customers');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (Gate::denies('manager')) {
            $userid = Auth::id();
            $customers = customer::find($id);
            if ($customers->user_id == $userid ) {
            return view('customers.edit', compact('customers')); 
            }
            else
            abort(403,"Sorry, you do not hold permission to edit this customer");
        }

        $customers = customer::find($id);
        return view('customers.edit', compact('customers')); 
       
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $customers = customer::findOrFail($id);
         $customers-> update($request->all());
             return redirect('customers');    
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (Gate::denies('manager')) {
            abort(403,"Sorry you are not allowed to delete!");
        }
        $customers = customer::find($id);
        $customers->delete();
        return redirect('customers');
    }

    public function deal($id)
    {
        if (Gate::denies('manager')) {
            abort(403,"Sorry you are not allowed to use this!");
        }
        $customers = customer::findOrFail($id);            
        $customers->status = 1; 
        $customers->save();
        return redirect('customers'); 
    }

}
