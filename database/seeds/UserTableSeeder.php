<?php

use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert(
            [
                [
                    'id'=>'1',
                    'name' => 'a',
                    'email' => 'a@a.com',
                    'password'=> Hash::make('12345678'),
                    'created_at' => date('Y,m,d G:i:s'),
                    'role' => 'manager',
                ],

                [
                    'id'=>'2',
                    'name' => 'b',
                    'email' => 'b@b.com',
                    'password'=> Hash::make('12345678'),
                    'created_at' => date('Y,m,d G:i:s'),
                    'role' => 'salesrep',
                ],

                [
                    'id'=>'3',
                    'name' => 'c',
                    'email' => 'c@c.com',
                    'password'=> Hash::make('12345678'),
                    'created_at' => date('Y,m,d G:i:s'),
                    'role' => 'salesrep',
                ],
            ]);
    }
}
