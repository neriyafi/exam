@extends('layouts.app')
@section('content')

<h1> Create a new Customer </h1>
<form method = 'post' action = "{{action('CustomersController@store')}}"  >
{{csrf_field()}}

<div class = "form-group">
    <!-- <label for = "id"> Customer id: </label>
    <input type = "number" class = "form-control" name = "id"> -->
    <label for = "name"> Customer name </label>
    <input type = "text" class = "form-control" name = "name">

    <label for = "email"> Customer email </label>
    <input type = "email" class = "form-control" name = "email">

     <label for = "phone"> Customer phone </label>
    <input type = "number" class = "form-control" name = "phone">
</div>

<div class = "form-group">
 <input type = "submit" class= "form-control" name="submit" value= "save">
</div>

<ul>
@foreach($errors->all() as $error)
   <li>{{$error}}</li>
@endforeach
</ul>

</form>

@endsection