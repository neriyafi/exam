@extends('layouts.app')
@section('content')


<h1> Edit Customer </h1>
<form method = 'post' action = "{{action('CustomersController@update', $customers->id)}}"  >
@csrf
@method('PATCH')
<div class = "form-group">

    <label for = "name"> name: </label>
    <input type = "text" class = "form-control" name = "name" value = "{{$customers->name}}">

    <label for = "email"> email:</label>
    <input type = "email" class = "form-control" name = "email" value = "{{$customers->email}}">

    <label for = "phone"> phone: </label>
    <input type = "number" class = "form-control" name = "phone" value = "{{$customers->phone}}">
</div>

<div class = "form-group">
 <input type = "submit" class= "form-control" name="submit" value= "Update">
</div>
</form>



@endsection