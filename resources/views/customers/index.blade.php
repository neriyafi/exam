<head>

<style>
    h1 {
      color: white;
      text-align: center;
      text-decoration: underline;
    }

       .stat1 {
      color: green;
    }
    #bold {
      font-weight:bold;
    }
    .form-1{
      width:10%;
      margin-left:50%;
      margin-right:50%
    }
    h3 {
      color: white;
      text-align: center;
    }
          table {
            margin-top: 50px;
            font-family: arial, sans-serif;
            border-collapse: collapse;
            width: 100%;
          }

          td, th {
            border: 1px solid #dddddd;
            text-align: left;
            padding: 8px;
          }

          tr:nth-child(even) {
            background-color: #dddddd;
      
          }
          tr:nth-child(odd){
            background-color: #aaaaaa;
      
          }
          tr:nth-child(even):hover {
            background-color: gray;
      
          }
          tr:nth-child(odd):hover {
            background-color: gray;
      
          }

</style>
</head>

@extends('layouts.app')
@section('content')
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<body>
<h1>customer list:</h1>


<!-- @if (Request::is('cars'))  
<h2><a href="{{action('CarController@LowtoHigh')}}"> low to high </a></h2>
@endif
@if (Request::is('LowtoHigh'))
<h2><a href="{{action('CarController@HigetoLow')}}"> high to low </a></h2>
@endif
@if (Request::is('HigetoLow'))
<h2><a href="{{action('CarController@LowtoHigh')}}"> low to high </a></h2>
@endif -->

          <a href = "{{route('customers.create') }}">  <h1> add a new customer </h1> </a>


                  <table>
                            <tr>
                                    <th>name</th>
                                    <th>mail</th>
                                    <th>phone</th>
                                    <th>edit</th>
                                    <th>delete</th> 
                                    <th>create by user</th>  
          @can('manager')           <th>status</th>   @endcan
                            </tr> 
                                @foreach($customers as $customer)

                                @if ($customer->status == 1 and $customer->user_id == $id )
                                <td class="stat1" id="bold" >{{$customer->name}}   </td>
                                <td class="stat1" id="bold" >{{$customer->email}}</td>
                                <td class="stat1" id="bold"  >{{$customer->phone}}</td>
                                <td class="stat1" id="bold"><a class="stat1" href="{{route('customers.edit',$customer->id)}}"> edit </a></td>
               @can('manager')  <td class="stat1" id="bold"> <a class="stat1" href="{{route('delete', $customer->id)}}">Delete</a>   </td>   @endcan
               @cannot('manager')  <td class="stat1"id="bold">  delete   </td>  @endcannot
                               <td class="stat1" id="bold">{{$customer->user_name}}</td>
           @can('manager')                <td class="stat1" id="bold">  </td>  @endcan
                            </tr>
                           
                            @elseif ($customer->user_id == $id and $customer->status != 1  )
                                <td  id="bold" >{{$customer->name}}   </td>
                                <td  id="bold" >{{$customer->email}}</td>
                                <td  id="bold"  >{{$customer->phone}}</td>
                                <td  id="bold"><a  href="{{route('customers.edit',$customer->id)}}"> edit </a></td>
               @can('manager')  <td  id="bold"> <a href="{{route('delete', $customer->id)}}">Delete</a>   </td>   @endcan
               @cannot('manager')  <td id="bold">  delete   </td>  @endcannot
                               <td  id="bold">{{$customer->user_name}}</td>
           @can('manager')     <td  id="bold">  <a href="{{action('CustomersController@deal', $customer->id )}}"> deal </a> </td>  @endcan
                            </tr>

                            
                            @elseif ($customer->status == 1 and $customer->user_id != $id   )
                                <td class="stat1" >{{$customer->name}}   </td>
                                <td class="stat1"  >{{$customer->email}}</td>
                                <td class="stat1"   >{{$customer->phone}}</td>
                                <td class="stat1" ><a class="stat1" href="{{route('customers.edit',$customer->id)}}"> edit </a></td>
               @can('manager')  <td class="stat1" > <a class="stat1" href="{{route('delete', $customer->id)}}">Delete</a>   </td>   @endcan
               @cannot('manager')  <td class="stat1">  delete   </td>  @endcannot
                               <td class="stat1" >{{$customer->user_name}}</td>
           @can('manager')                <td class="stat1" >  </td>  @endcan
                            </tr>

                            @else
                            <td  >{{$customer->name}}   </td>
                                <td>{{$customer->email}}</td>
                                <td>{{$customer->phone}}</td>
                                <td><a  href="{{route('customers.edit',$customer->id)}}"> edit </a></td>
               @can('manager')  <td> <a href="{{route('delete', $customer->id)}}">Delete</a>   </td>   @endcan
               @cannot('manager')  <td >  delete   </td>  @endcannot
                               <td  >{{$customer->user_name}}</td>
             @can('manager')                <td  id="bold">  <a href="{{action('CustomersController@deal', $customer->id )}}"> deal </a> </td>  @endcan
                            </tr>

                            @endif

                                @endforeach
                              
                </table>

                
@endsection

  
     